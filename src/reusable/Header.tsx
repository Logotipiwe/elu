import React from 'react';
import './Header.scss'
import { cn } from '../classname';
import { imgSrc } from '../img';

class Header extends React.Component<{goBack?: ()=>void}, any> {
	render() {
		const goBackFunc = this.props.goBack ? this.props.goBack : undefined;
		const blockClass = cn('Header');
		return (
			<div className={blockClass()}>
				{goBackFunc ? <img src={imgSrc("back_arrow.svg")} alt="" onClick={goBackFunc}/> : <div/>}
				<img src={imgSrc("header_center.svg")} alt="" style={{height: '100%'}}/>
				<div/>
			</div>
		);
	}
}

export default Header;
