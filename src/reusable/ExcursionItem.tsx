import React from 'react';
import {inject, observer} from "mobx-react";
import './Excursion.scss'
import RootStore from '../stores/RootStore';
import {cn} from '../classname';
import {imgSrc} from "../img";
import StarSvg from "../svg/StarSvg";
import Excursion from "../models/Excursion";
import HeartSvg from "../svg/HeartSvg";

@inject("RootStore")
@observer
class ExcursionItem extends React.Component<{ RootStore?: RootStore, excursion: Excursion }, any> {
	render() {
		const excursion = this.props.excursion;
		const blockClass = cn('Excursion');
		return (
			<div
				className={blockClass()}
				style={{'backgroundImage': `url(${(imgSrc(`excursions/${excursion.id}.png`))})`}}
			>
				{excursion.isLiked && <div onClick={excursion.onLikeClick} className={blockClass('like')}><HeartSvg/></div>}
				<div className={blockClass('rating')}>{new Array(5).fill(0).map((x, i) =>
					<div key={i} className={blockClass('star_rate', {checked: (i + 1) <= excursion.rating})}><StarSvg/></div>
				)}</div>
				<div className={blockClass('desc')}>{excursion.descShort}</div>
				<div className={blockClass('price')}>ОТ {excursion.price} РУБ</div>
				<div className={blockClass('price_type')}>за человека</div>
			</div>
		);
	}
}

export default ExcursionItem;
