import React, {CSSProperties, MouseEventHandler} from 'react';
import './Button.scss'
import { cn } from '../classname';

class Button extends React.Component<{onClick?: MouseEventHandler, isGreen?: boolean, style?: CSSProperties}, any> {
	render() {
		const blockClass = cn('Button');
		return (
			<div style={this.props.style} className={blockClass('',{green: Boolean(this.props.isGreen)})} onClick={this.props.onClick ?? undefined}>{this.props.children}</div>
		);
	}
}

export default Button;
