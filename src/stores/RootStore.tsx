import {action, observable} from "mobx";
import {RouterStore} from "mobx-react-router";
import React from "react";
import Excursion from "../models/Excursion";
import Chat from "../models/Chat";

class RootStore {
	constructor(RouterStore: RouterStore) {
		this.RouterStore = RouterStore;

		const excursions: IExcursion[] = [
			{
				id: 1,
				title: 'Автобусная обзорная экскурсия по Москве',
				descShort: <>Вы увидите и сфотографируете<br/>многие знакомые места Москвы:<br/>
					Красную площадь,улицу и многое другое.</>,
				desc: "Вы отправитесь на знакомство\n" +
					"со столицей нашей родины – Москвой. \n" +
					"Один из древнейших городов\n" +
					"Европы гармонично сочетает в себе \n" +
					"прекрасно сохранившиеся церкви и храмы \n" +
					"с современными небоскребами. \n",
				rating: 4,
				price: 980,
			},
			{
				id: 2,
				title: 'Экскурсия по Питеру',
				descShort: <>Вы увидите и сфотографируете<br/>многие знакомые места Питера:<br/>
					Дворцовую площадь, улицу и многое другое.</>,
				desc: "Вы отправитесь на знакомство\n" +
					"с северной столицей нашей родины – Петербургом. \n" +
					"Один из древнейших городов\n" +
					"Европы гармонично сочетает в себе \n" +
					"прекрасно сохранившиеся церкви и храмы \n" +
					"с современными небоскребами. \n",
				rating: 3,
				price: 2500,
			},
			{
				id: 3,
				title: 'Кронштадт - обзорная экскурсия',
				descShort: <>Вы увидите и сфотографируете<br/>многие места любимого Кронштадта:<br/>
					и многое другое.</>,
				desc: "Красота и атмосфера Старого города перенесут путешественника в далёкие времена прошлого, когда весь мир славился кронштадтцами." +
					"Кронштадтские экскурсии — это приморский воздух, пейзажи Финского залива и множество уникальных достопримечательностей которые оставят исключительное впечатление о поездке в наш город.",
				rating: 5,
				price: 899,
			},
			{
				id: 4,
				title: 'Экскурсия в древний город Болгар',
				descShort: <>Загородная автобусная экскурсия в Болгар – прекрасная возможность прикоснуться к древним истокам Татарстана.</>,
				desc: "Болгар – это культовое и очень важное место для татар, ведь именно здесь был принят ислам в 922 г. Сегодня это обширное городище, куда приезжает множество туристов и паломников. Уникальное историческое место, где расположены архитектурные памятники XIII – XIV веков.\n" +
					"Болгарский историко-архитектурный комплекс самый северный в мире памятник средневекового мусульманского зодчества, включен в список всемирного наследия ЮНЕСКО.",
				rating: 4,
				price: 15999,
			},
		];
		this.excursions = excursions.map(e=>new Excursion(e));

		const chats: IChat[] = [
			{
				id: 1,
				title: "Экскурсовод",
				messages: [
					{body: "Здравствуйте, Иван! Мы ожидаем вас на Московских Воротах, Нулевой туристический километр. Благодарим, что вы выбрали именно нас!"},
					{body: "Если появятся вопросы, пишите сюда, мы вам обязательно ответим."},
					{body: "Я уже подхожу, скоро буду", isFromMe: true}
				]
			},
			{
				id: 2,
				title: "Тур. гид",
				messages: [
					{body: "Здравствуйте, а во сколько планируется экскурсия?", isFromMe: true}
				]
			}
		];

		this.chats = chats.map(c=>new Chat(c));
	}

	RouterStore: RouterStore;

	isDev: boolean = (process.env.NODE_ENV === "development");

	@observable appState: appStates = "INITIALIZING";

	@observable mainDefaultRoute = this.isDev
		? "/map"
		: "/map";

	@observable excursions: Excursion[];
	@observable chats: Chat[];

	getExcursion(id: number): Excursion{
		const found = this.excursions.filter(e=>e.id === id);
		return (found) ? found[0] : this.excursions[0];
	}

	getChat(id: number): Chat {
		const found = this.chats.filter(e=>e.id === id);
		return (found) ? found[0] : this.chats[0];
	}

	@action.bound setAppState(state: appStates) {
		const mapStateToRoute: Record<appStates, string> = {
			DEFAULT: '/',
			UNAUTH: '/auth',
			INITIALIZING: '/init'
		};
		console.log('state to '+state);
		this.RouterStore.push(mapStateToRoute[state]);
		this.appState = state;
	}
}
export default RootStore;
