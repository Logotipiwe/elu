import React from 'react';
import {inject, observer} from "mobx-react";
import './App.scss'
import RootStore from './stores/RootStore';
import {cn} from './classname';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import {RouterStore} from 'mobx-react-router';
import Init from "./routes/Init";
import Main from './routes/Main';
import Auth from './routes/Auth';

@inject("RootStore")
@inject("routing")
@observer
class App extends React.Component<{ RootStore?: RootStore, routing?: RouterStore }, any> {
	componentDidMount(): void {
		const RootStore = this.props.RootStore!;
		const state: appStates = RootStore.isDev ? "DEFAULT" : "UNAUTH";
		setTimeout(RootStore.setAppState.bind(null, state), RootStore.isDev ? 100 : 2000);
	}

	render() {
		const blockClass = cn('App');
		const RootStore = this.props.RootStore!;
		// const {location, push, goBack} = this.props.routing!;
		return (
			<div id={blockClass()}>
				{RootStore.appState === "INITIALIZING" && <Redirect to={'/init'}/>}
				<Switch>
					<Route path={'/init'} exact component={Init}/>
					<Route path={'/auth'} component={Auth}/>
					<Route path={'/'} component={Main}/>
				</Switch>
			</div>
		);
	}
}

export default App;
