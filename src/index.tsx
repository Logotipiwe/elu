import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "mobx-react";
import RootStore from "./stores/RootStore";
import {RouterStore, syncHistoryWithStore} from "mobx-react-router";
import history from "./history";
import { Router } from 'react-router';

const routing = new RouterStore();
const browserHistory = syncHistoryWithStore(history, routing);
const stores = {
	RootStore: new RootStore(routing),
	routing
};

ReactDOM.render(
	<Provider {...stores}>
		<React.StrictMode>
      <Router history={browserHistory}>
        <App/>
      </Router>
		</React.StrictMode>
	</Provider>,
	document.getElementById('root')
);
