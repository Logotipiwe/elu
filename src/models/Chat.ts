import {action, observable} from "mobx";

export default class Chat {
	constructor(chat: IChat) {
		this.id = chat.id;
		this.title = chat.title;
		this.messages = chat.messages;
	}

	@observable id: number;
	@observable title: string;
	@observable messages: IMessage[];

	@action.bound newMessage(body: string){
		this.messages.push({body, isFromMe: true});
	}
}
