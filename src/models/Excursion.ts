import {action, observable} from "mobx";

export default class Excursion {
	constructor(excursion: IExcursion) {

		this.id = excursion.id;
		this.title = excursion.title;
		this.descShort = excursion.descShort;
		this.desc = excursion.desc;
		this.price = excursion.price;
		this.rating = excursion.rating;

	}

	@observable id: number;
	@observable title: string;
	@observable descShort: string | JSX.Element;
	@observable desc: string | JSX.Element;
	@observable price: number;
	@observable rating: number;

	@observable isLiked: boolean = (process.env.NODE_ENV === 'development');
	@observable isReserved: boolean = false;
	@observable reserveDate: undefined|Date;

	@action.bound onLikeClick(){
		this.isLiked = !this.isLiked;
	}

	@action.bound onReserveClick(){
		setTimeout(()=> {
			this.isReserved = !this.isReserved;
		},500);
	}
}
