import React from 'react';
import {inject, observer} from "mobx-react";
import './History.scss'
import RootStore from '../../stores/RootStore';
import { cn } from '../../classname';

@inject("RootStore")
@observer
class History extends React.Component<{ RootStore?: RootStore }, any> {
	render() {
		const blockClass = cn('History');
		return (
			<div id={blockClass()}>
				<h1>Здесь будет выводиться история Ваших экскурсий</h1>
			</div>
		);
	}
}

export default History;
