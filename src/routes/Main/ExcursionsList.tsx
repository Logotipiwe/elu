import React from 'react';
import {inject, observer} from "mobx-react";
import './ExcursionsList.scss'
import RootStore from '../../stores/RootStore';
import {cn} from '../../classname';
import {imgSrc} from "../../img";
import StarSvg from "../../svg/StarSvg";
import {match, Route, RouteComponentProps, Switch} from "react-router";
import {RouterStore} from "mobx-react-router";
import SelectedExcursion from "./SelectedExcursion";
import ExcursionItem from "../../reusable/ExcursionItem";

@inject("RootStore")
@inject("routing")
@observer
class ExcursionsList extends React.Component<{ RootStore?: RootStore, routing: RouterStore, match: match }, any> {
	render() {
		const blockClass = cn("ExcursionsList");
		const RootStore = this.props.RootStore!;
		const match = this.props.match;
		const {location, push, replace} = this.props.routing;
		return (
			<div id={blockClass()}>
				<Switch>
					<Route path={this.props.match.path} exact>
					{RootStore.excursions.map(excursion => {
						return (
							<a onClick={() => push(`${match.path}/${excursion.id}`)}>
								<ExcursionItem excursion={excursion}/>
							</a>
						)
					})}
					</Route>
					<Route path={`${match.path}/:excursion_id`} component={(props: RouteComponentProps)=>{
						const match = props.match as match<{excursion_id: string}>;
						const excursion = RootStore.getExcursion(parseInt(match.params.excursion_id));
						return (
							<SelectedExcursion excursion={excursion}/>
						)
					}}/>
				</Switch>
			</div>
		);
	}
}

export default ExcursionsList;
