import React from 'react';
import {inject, observer} from "mobx-react";
import './SelectedChat.scss'
import RootStore from '../../../stores/RootStore';
import { cn } from '../../../classname';
import {imgSrc} from "../../../img";
import Chat from "../../../models/Chat";

@inject("RootStore")
@observer
class SelectedChat extends React.Component<{ RootStore?: RootStore, chat: Chat }, any> {
	constructor(props: any) {
		super(props);
		this.state = {input: ''}
	}

	render() {
		const selectChat = this.props.chat;
		const chatClass = cn('SelectedChat');
		return (
			<div className={chatClass()}>
				<div className={chatClass('title')}>{selectChat.title}</div>
				<div className={chatClass("messages")}>
					{selectChat.messages.map(msg=>{

						const backgroundImage = (msg.isFromMe)
							? `url(${imgSrc('avatar.png')})`
							: `url(/elu/img/chats/${selectChat.id}.png)`;
						console.log(backgroundImage);
						return (
							<div className={chatClass('message_item', {from_me: msg.isFromMe})}>
								<div className={chatClass('avatar')} style={{backgroundImage}}/>
								<div className={chatClass('msg_body')}>{msg.body}</div>
							</div>
						)
					})}
				</div>
				<div className={chatClass('input')}>
					<div>
						<input placeholder="Печатать..." value={this.state.input} onChange={e=>this.setState({input: e.target.value})}/>
						<div
							style={{transform: 'rotate(180deg)'}}
						>
							<img src={imgSrc('msg_send.svg')} onClick={selectChat.newMessage.bind(null, this.state.input)}/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default SelectedChat;
