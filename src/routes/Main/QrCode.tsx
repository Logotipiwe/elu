import React from 'react';
import {inject, observer} from "mobx-react";
import './QrCode.scss'
import RootStore from '../../stores/RootStore';
import {cn} from '../../classname';

@inject("RootStore")
@observer
class QrCode extends React.Component<{ RootStore?: RootStore }, any> {
	render() {
		const blockClass = cn('QrCode');
		return (
			<div id={blockClass()}>
				<h1>Функционал доступен только в мобильном приложении</h1>
			</div>
		);
	}
}

export default QrCode;
