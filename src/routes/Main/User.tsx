import React from 'react';
import {inject, observer} from "mobx-react";
import './User.scss'
import RootStore from '../../stores/RootStore';
import {cn} from '../../classname';
import Likes from "./User/Likes";
import UserFilter from "./User/UserFilter";
import UserSettings from "./User/UserSettings";
import {match, Route, Switch} from "react-router";
import {RouterStore} from "mobx-react-router";

@inject("RootStore")
@inject("routing")
@observer
class User extends React.Component<{ RootStore?: RootStore, routing?: RouterStore, match: match }, any> {
	render() {
		const blockClass = cn('User');
		const {location, push} = this.props.routing!;
		const match = this.props.match;
		const routes: IRoute<React.ComponentType>[] = [
			{
				key: 'likes',
				title: "Избранные экскурсии",
				component: Likes
			},
			{
				key: 'filters',
				title: "Фильтры",
				component: UserFilter
			},
			{
				key: 'settings',
				title: "Настройки профиля",
				component: UserSettings
			},
		];
		return (
			<div id={blockClass()}>
				<Switch>
					<Route path={`${match.path}`} exact>
						<div className={blockClass('blur')}/>
						<div className={blockClass('info')} style={{backgroundImage: "url(img/user_background.png)"}}>
							<div className={blockClass('avatar')} style={{backgroundImage: "url(img/avatar.png"}}/>
							<div className={blockClass('info_data')}>
								<div className={blockClass('info_name')}>Иван</div>
								<div className={blockClass('info_money')}>баланс: 77р</div>
								<div className={blockClass('add_money')}>
									<div>+</div>
									<span>Пополнить</span>
								</div>
							</div>
						</div>
						<div className={blockClass('menu')}>
							{routes.map(r =>
								<div key={r.key} onClick={() => push(`${match.path}/${r.key}`)}>{r.title}</div>
							)}
						</div>
					</Route>
					{routes.map(r=>
						<Route key={r.key} path={`${match.path}/${r.key}`} component={r.component}/>
					)}
				</Switch>
			</div>
		);
	}
}

export default User;
