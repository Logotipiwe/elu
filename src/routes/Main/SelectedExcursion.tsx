import React from 'react';
import {inject, observer} from "mobx-react";
import './SelectedExcursion.scss'
import RootStore from '../../stores/RootStore';
import { cn } from '../../classname';
import Excursion from "../../models/Excursion";
import HeartSvg from "../../svg/HeartSvg";
import {imgSrc} from "../../img";
import StarSvg from "../../svg/StarSvg";
import Button from "../../reusable/Button";

@inject("RootStore")
@observer
class SelectedExcursion extends React.Component<{ RootStore?: RootStore, excursion: Excursion }, any> {
	render() {
		const blockClass = cn('SelectedExcursion');
		const excursion = this.props.excursion;
		return (
			<div id={blockClass()}>
				<div
					className={blockClass('header_img')}
					style={{backgroundImage: `url(${imgSrc(`excursions/${excursion.id}.png`)})`}}
				>
					<div className={blockClass('header_img_title')}>{excursion.title}</div>
					<div
						onClick={excursion.onLikeClick}
						className={blockClass('like', {checked: excursion.isLiked})}
					>
						<HeartSvg/>
					</div>
				</div>
				<div className={blockClass('info')}>
					<div className={blockClass('info_price')}>ОТ {excursion.price} РУБ</div>
					<div className={blockClass('info_rating')}>
						{new Array(5).fill(0).map((x,i)=>
							<div className={blockClass('star', {checked: (i+1) <= excursion.rating})}><StarSvg/></div>
						)}
					</div>
				</div>
				<Button isGreen={excursion.isReserved} onClick={excursion.onReserveClick}>
					{(excursion.isReserved) ? "Заказ оформлен" : "Оформить заказ"}
				</Button>
				<div className={blockClass('desc')}>
					{excursion.desc}
				</div>
			</div>
		);
	}
}

export default SelectedExcursion;
