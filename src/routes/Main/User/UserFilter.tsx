import React from 'react';
import {inject, observer} from "mobx-react";
import './UserFilter.scss'
import RootStore from '../../../stores/RootStore';
import {cn} from "../../../classname";
import Button from "../../../reusable/Button";

@inject("RootStore")
@observer
class UserFilter extends React.Component<{ RootStore?: RootStore }, any> {
	constructor(props: any) {
		super(props);
		this.state = {
			filters: [false,true,false]
		};
		this.onClickFilter = this.onClickFilter.bind(this);
	}

	onClickFilter(i: number){
		console.log('kik');
		this.setState((s: any) => {
			let newState = Object.assign({}, s);
			newState.filters[i] = !newState.filters[i];
			console.log(newState.filters);
			return {...newState, filters: newState.filters}
		});
	}

	render() {
		const blockClass = cn('UserFilter');
		return (
			<div id={blockClass()}>
				<div className={blockClass('label')}>Город, населенный пункт</div>
				<input className={blockClass('input')}/>
				<div className={blockClass('label')}>Тип экскурсий</div>
				<input className={blockClass('input')}/>
				<div className={blockClass('label')}>Радиус поиска, км</div>
				<input className={blockClass('input')}/>
				<div className={blockClass('label')}>Цена за человека</div>
				<div className={blockClass('row')}>
					<span>от</span>
					<input className={blockClass('input')}/>
					<span>до</span>
					<input className={blockClass('input')}/>
				</div>
				<div className={blockClass('li')} onClick={this.onClickFilter.bind(null, 0)}>
					<span>Экскурсии групповые</span>
					<div className={blockClass('checkbox', {checked: this.state.filters[0]})}/>
				</div>
				<div className={blockClass('li')} onClick={this.onClickFilter.bind(null, 1)}>
					<span>Экскурсии индивидуальные</span>
					<div className={blockClass('checkbox', {checked: this.state.filters[1]})}/>
				</div>
				<div className={blockClass('li')} onClick={this.onClickFilter.bind(null, 2)}>
					<span>С детьми</span>
					<div className={blockClass('checkbox', {checked: this.state.filters[2]})}/>
				</div>
				<Button style={{width: 250, alignSelf: 'center', marginTop: 25}}>Применить</Button>
			</div>
		);
	}
}

export default UserFilter;
