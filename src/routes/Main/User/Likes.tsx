import React from 'react';
import {inject, observer} from "mobx-react";
import './Likes.scss'
import RootStore from '../../../stores/RootStore';
import { cn } from '../../../classname';
import ExcursionItem from "../../../reusable/ExcursionItem";

@inject("RootStore")
@observer
class Likes extends React.Component<{ RootStore?: RootStore }, any> {
	render() {
		const blockClass = cn('Likes');
		const liked = this.props.RootStore!.excursions.filter(e=>e.isLiked);
		console.log(liked.length);
		return (
			<div id={blockClass()}>
				{liked.map(excursion=>
					<ExcursionItem key={excursion.id} excursion={excursion}/>
				)}
			</div>
		);
	}
}

export default Likes;
