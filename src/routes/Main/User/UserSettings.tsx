import React from 'react';
import {inject, observer} from "mobx-react";
import RootStore from '../../../stores/RootStore';
import { cn } from '../../../classname';
import "./UserSettings.scss"

@inject("RootStore")
@observer
class UserSettings extends React.Component<{ RootStore?: RootStore }, any> {
	render() {
		const blockClass = cn('UserSettings');
		return (
			<div id={blockClass()}>
				<div className={blockClass('avatar')} style={{backgroundImage: "url(/elu/img/avatar.png"}}/>
				<div className={blockClass('list')}>
					<div>Фото профиля</div>
					<div>Имя профиля</div>
					<div>Платежная информация</div>
					<div>Электронная почта</div>
					<div>Телефонный номер</div>
					<div>Доступ к геолокации</div>
				</div>
			</div>
		);
	}
}

export default UserSettings;
