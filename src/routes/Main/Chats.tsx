import React from 'react';
import {inject, observer} from "mobx-react";
import './Chats.scss'
import RootStore from '../../stores/RootStore';
import {cn} from '../../classname';
import {match, Route, RouteComponentProps, Switch} from "react-router";
import {RouterStore} from "mobx-react-router";
import {imgSrc} from "../../img";
import SelectedChat from "./Chats/SelectedChat";

@inject("RootStore")
@inject("routing")
@observer
class Chats extends React.Component<{ RootStore?: RootStore, match: match, routing: RouterStore }, any> {
	constructor(props: any) {
		super(props);
		this.state = {input: ''}
	}

	render() {
		const c = cn('Chats');
		const RootStore = this.props.RootStore!;
		const match = this.props.match;
		const {push} = this.props.routing;
		return (
			<div id={c()}>
				<Switch>
					<Route path={`${match.path}`} exact>
						{RootStore.chats.map(chat => {
							const lastMessage = (chat.messages.length) ? chat.messages[chat.messages.length - 1] : {body: '...'};
							const placeholder = ((lastMessage.isFromMe) ? "Вы: " : '') + lastMessage.body;
							return (
								<div className={c('chat_item')} onClick={() => push(`${match.path}/${chat.id}`)}>
									<div className={c('chat_img')} style={{backgroundImage: `url(/elu/img/chats/${chat.id}.png)`}}/>
									<div className={c('chat_data')}>
										<div className={c('row1')}>
											<div className={c('title')}>{chat.title}</div>
											<div className={c('time')}>17.06.2019 14:10</div>
										</div>
										<div className={c('row2')}>{placeholder}</div>
									</div>
								</div>
							)
						})}
					</Route>
					<Route path={`${match.path}/:chat_id`} component={(props: RouteComponentProps<{ chat_id: string }>) => {
						const selectChat = RootStore.getChat(parseInt(props.match.params.chat_id));
						const chatClass = cn("SelectedChat");
						return (
							<SelectedChat chat={selectChat}/>
						)
					}}/>
				</Switch>
			</div>
		);
	}
}

export default Chats;
