import React from 'react';
import {inject, observer} from "mobx-react";
import './Main.scss'
import RootStore from '../stores/RootStore';
import {cn} from '../classname';
import {RouterStore} from "mobx-react-router";
import {match, Route} from "react-router";
import History from "./Main/History";
import Header from "../reusable/Header";
import MapSvg from "../svg/tabbar/MapSvg";
import HistorySvg from "../svg/tabbar/HistorySvg";
import ChatSvg from "../svg/tabbar/ChatSvg";
import QrSvg from "../svg/tabbar/QrSvg";
import UserSvg from "../svg/tabbar/UserSvg";
import ExcursionsList from "./Main/ExcursionsList";
import User from "./Main/User";
import QrCode from "./Main/QrCode";
import Chats from "./Main/Chats";

@inject("RootStore")
@inject("routing")
@observer
class Main extends React.Component<{ RootStore?: RootStore, routing?: RouterStore, match: match }, any> {
	componentDidMount() {
		const {location, replace} = this.props.routing!;
		if(location.pathname === '/') replace(this.props.RootStore!.mainDefaultRoute);
		console.log(location.pathname);
	}

	render() {
		const blockClass = cn('Main');
		const RootStore = this.props.RootStore!;
		const match = this.props.match;
		const {location, push, replace, goBack} = this.props.routing!;
		const routes: IMainRoute<React.ComponentType>[]  = [
			{
				key: 'history',
				title: 'история',
				component: History,
				img: HistorySvg()
			},
			{
				key: 'chats',
				title: 'чаты',
				component: Chats,
				img: ChatSvg()
			},
			{
				key: 'map',
				title: 'карта',
				component: ExcursionsList,
				img: MapSvg()
			},
			{
				key: 'qr',
				title: 'код',
				component: QrCode,
				img: QrSvg()
			},
			{
				key: 'user',
				title: 'пользователь',
				component: User,
				img: UserSvg()
			},
		];
		const isOnMainRoute = routes.map(r=>`/${r.key}`).includes(location.pathname);
		return (
			<div id={blockClass()}>
				<Header goBack={
					(!isOnMainRoute) ? ()=>goBack() : undefined}/>
				<div className={blockClass('main')}>
					{routes.map(r=>{
						return (
							<Route key={r.key} path={`/${r.key}`} component={r.component}/>
						)
					})}
				</div>
				<div
					className={blockClass("tabbar", {hidden: !isOnMainRoute})}
				>
					{routes.map(r=>{
						return (
							<div
								onClick={()=>replace(`/${r.key}`)}
								key={r.key}
								className={blockClass(
									'tabbar_item',
									{selected: location.pathname === `/${r.key}`}
									)}
							>
								{r.img}
							</div>
						)
					})}
				</div>
			</div>
		);
	}
}

export default Main;
