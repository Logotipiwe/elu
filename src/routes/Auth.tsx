import React from 'react';
import {inject, observer} from "mobx-react";
import './Auth.scss'
import RootStore from "../stores/RootStore";
import {cn} from '../classname';
import {match, Route, Switch} from "react-router";
import {RouterStore} from "mobx-react-router";
import Header from "../reusable/Header";
import Button from '../reusable/Button';
import { imgSrc } from '../img';

@inject("RootStore")
@inject("routing")
@observer
class Auth extends React.Component<{ RootStore?: RootStore, routing?: RouterStore, match: match }, any> {
	componentDidMount(): void {
		const routing = this.props.routing!;
		const {location, replace} = routing;
		if (location.pathname === '/auth') replace('/auth/login')
	}

	render() {
		const {match} = this.props;
		const routing = this.props.routing!;
		const {location, replace, push, goBack} = routing;
		const blockClass = cn('Auth');
		console.log(location);
		return (
			<div id={blockClass()}>
				<Header goBack={((location.pathname === "/auth/reg") ? ()=>goBack() : undefined)}/>
				<div className={blockClass('body')}>
					<Switch>
						<Route path={`${match.path}/login`}>
							<div
								className={blockClass('field')}
								style={{marginTop: 50}}
							>
								<span>Mail</span>
								<input key='mail'/>
							</div>
							<div className={blockClass('field')}>
								<span>Пароль</span>
								<input type='password' key='password'/>
							</div>
							<div className={blockClass('socials')}>
								<img src={imgSrc('soc_twitter.svg')}/>
								<img src={imgSrc('soc_facebook.svg')}/>
								<img src={imgSrc('soc_vk.svg')}/>
							</div>
							<Button onClick={()=>push('/')}>Войти</Button>
							<div style={{marginTop: 21}}/>
							<Button onClick={()=>push(`${match.path}/reg`)}>Регистрация</Button>
						</Route>
						<Route path={`${match.path}/reg`}>
							<div
								className={blockClass('field')}
								style={{marginTop: 50}}
							>
								<span>Имя</span>
								<input key='name'/>
							</div>
							<div className={blockClass('field')}>
								<span>Mail</span>
								<input key='mail'/>
							</div>
							<div className={blockClass('field')}>
								<span>Пароль</span>
								<input type='password' key='password'/>
							</div>
							<div style={{marginTop: 50}}/>
							<Button onClick={()=>push('/')}>Зарегистрироваться</Button>
						</Route>
					</Switch>
				</div>
			</div>
		);
	}
}

export default Auth;
