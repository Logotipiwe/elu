import React from 'react';
import {inject, observer} from "mobx-react";
import './Init.scss'
import RootStore from '../stores/RootStore';
import { cn } from '../classname';

@inject("RootStore")
@observer
class Init extends React.Component<{RootStore?: RootStore}, any>{
	render() {
	    const blockClass = cn('Init');
		return (
			<div id={blockClass()}>
				<img src='img/init_center.svg'/>
				<img src='img/init1.svg' className={blockClass("init_img", {top: true, left: false})}/>
				<img src='img/init2.svg' className={blockClass("init_img", {top: true, left: true})}/>
				<img src='img/init3.svg' className={blockClass("init_img", {top: false, left: false})}/>
				<img src='img/init4.svg' className={blockClass("init_img", {top: false, left: true})}/>
			</div>
		);
	}
}

export default Init;
