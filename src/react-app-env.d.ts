/// <reference types="react-scripts" />
type appStates = "INITIALIZING" | "UNAUTH" | "DEFAULT"
interface IRoute<T=JSX.Element>{
	key: string,
	title: string,
	component: T
}
interface IMainRoute<T=JSX.Element> extends IRoute{
	img: JSX.Element,
	component: T
}
interface IExcursion {
	id: number,
	title: string,
	descShort: string | JSX.Element,
	desc: string | JSX.Element,
	price: number,
	rating: number,
}
interface IChat {
	id: number,
	title: string,
	messages: IMessage[]
}
interface IMessage {
	body: string,
	isFromMe?: boolean
}
